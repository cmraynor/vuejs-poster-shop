
new Vue({
	el: "#app",
	data: {
		total: 0,
		items: [],
		cart: [],
		newSearch: 'anime',
		lastSearch: '',
		loadNum: 2,
		results: '',
		loading: false,
		price: 9.99
	},
	computed: {
		noMoreResults: function () {
			return this.results.length === this.items.length && this.results.length > 0;
		}
	},
	methods: {
		appendItems: function () {
			if (this.items.length < this.results.length) {
				var self = this;
				var append = this.results.slice(this.items.length, this.items.length + this.loadNum);
				this.items = this.items.concat(append);
			}
		},
		onSubmit: function () {
			if(this.newSearch.length) {
				this.items = [];
				this.loading = true;
				this.$http.get('/search/'.concat(this.newSearch)).then(function (res) {
					this.lastSearch = this.newSearch;
					this.results = res.data;
					this.items = res.data.slice(0, self.loadNum);
					this.loading = false;
				});
			}
		},
		addItem: function (index) {
			var item = this.items[index];
			this.total += this.price;
			var itemInCart = false;
			//loop through cart items to see if item is already in cart
			for (var i = 0; i < this.cart.length; i++) {
				if (this.cart[i].id === item.id) {
					itemInCart = true;
					//add one more to item cart qty
					this.cart[i].qty++;
					break; //no need to continue looping if match found
				}
			}
			if (!itemInCart) {
				//place item in cart
				this.cart.push({
					id: item.id,
					title: item.title,
					qty: 1,
					price: this.price
				});
			}
		},
		inc: function (item) {
			item.qty++;
			this.total += this.price;
		},
		dec: function (item) {
			item.qty--;
			this.total -= item.price;
			if (item.qty <= 0) {
				for (var i = 0; i < this.cart.length; i++) {
					if (this.cart[i].id == item.id) {
						this.cart.splice(i, 1);
					}
				}
			}
		}
	},
	filters: {
		currency: function (price) {
			console.log(price);
			return '$'.concat(price.toFixed(2));
		}
	},
	mounted: function() {
		this.onSubmit();
		var elem = document.getElementById('product-list-bottom');
		var vueInstance = this;
		var watcher = scrollMonitor.create(elem);
		watcher.enterViewport(function() {
			vueInstance.appendItems();
		});
	}
});
