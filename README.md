
# Vue.js Poster Shop project from the Ultimate Vue.js Developers Course

Source code for the [Ultimate Vue.js Developers Course](http://bit.ly/2mPK8ny).

Project is now completed and currently working on refining and expanding it.

Completed project demo online at:

https://tranquil-dusk-68323.herokuapp.com/